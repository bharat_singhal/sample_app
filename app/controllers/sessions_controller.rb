class SessionsController < ApplicationController
  def new
  end

  def create 
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		if user.activated? # it is a boolean field called 'activated', activated? is provided by default
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end

  	else 
  		# show error page
  		# We are using flash.now as it is used with render
  		flash.now[:danger] = "Invalid Username/Password" 
  		render 'new' 
    end
   end

   def destroy
   	log_out if logged_in?
   	redirect_to root_url
    	
    end 
end
